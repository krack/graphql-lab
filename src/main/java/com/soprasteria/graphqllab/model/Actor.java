package com.soprasteria.graphqllab.model;

import lombok.Data;

@Data
public class Actor {
    private String id;
    private String name;
}