package com.soprasteria.graphqllab.model;

import lombok.Data;

@Data
public class Director {
    private String id;
    private String name;
}