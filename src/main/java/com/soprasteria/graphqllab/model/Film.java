package com.soprasteria.graphqllab.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Film {
    private String id;
    private String title;
    private int releaseYear;
    private String directorId;
    private List<String> actors;

    public Film(){
        this.actors = new ArrayList<>();
    }
}