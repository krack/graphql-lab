package com.soprasteria.graphqllab;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import com.soprasteria.graphqllab.model.Actor;
import com.soprasteria.graphqllab.model.Director;
import com.soprasteria.graphqllab.model.Film;
import com.soprasteria.graphqllab.model.NoteReference;

@Controller
public class FilmQuery {

    private ExecutorService executor = Executors.newFixedThreadPool(10);
    private List<Film> films;
    private List<Director> directors;
    private List<Actor> actors;

    public FilmQuery() {
        // create director
        this.directors = new ArrayList<>();
        int indexDirector = 0;
        {
            Director director = new Director();
            director.setId(++indexDirector + "");
            director.setName("Peter Jackson");
            this.directors.add(director);
        }
        {
            Director director = new Director();
            director.setId(++indexDirector + "");
            director.setName("Gérard Oury");
            this.directors.add(director);
        }
        {
            Director director = new Director();
            director.setId(++indexDirector + "");
            director.setName("Kelly Asbury");
            this.directors.add(director);
        }

        {
            Director director = new Director();
            director.setId(++indexDirector + "");
            director.setName("Joel Schumacher");
            this.directors.add(director);
        }

        {
            Director director = new Director();
            director.setId(++indexDirector + "");
            director.setName("Yann Samuell");
            this.directors.add(director);
        }

        // create director
        this.actors = new ArrayList<>();
        int actors = 0;
        {
            Actor actor = new Actor();
            actor.setId(++actors + "");
            actor.setName("Elijah Wood");
            this.actors.add(actor);
        }
        {
            Actor actor = new Actor();
            actor.setId(++actors + "");
            actor.setName("Cate Blanchett");
            this.actors.add(actor);
        }
        {
            Actor actor = new Actor();
            actor.setId(++actors + "");
            actor.setName("Sean Astin");
            this.actors.add(actor);
        }

        // create films
        this.films = new ArrayList<>();
        int indexFilms = 0;
        {
            Film film = new Film();
            film.setId(++indexFilms + "");
            film.setTitle("Le Seigneur des anneaux : La Communauté de l'anneau");
            film.setReleaseYear(2001);
            film.setDirectorId(this.directors.get(0).getId());
            film.getActors().add(this.actors.get(0).getId());
            film.getActors().add(this.actors.get(1).getId());
            film.getActors().add(this.actors.get(2).getId());
            this.films.add(film);
        }
        {
            Film film = new Film();
            film.setId(++indexFilms + "");
            film.setTitle("Le Seigneur des anneaux : Les Deux Tours");
            film.setReleaseYear(2002);
            film.setDirectorId(this.directors.get(0).getId());
            film.getActors().add(this.actors.get(0).getId());
            film.getActors().add(this.actors.get(1).getId());
            film.getActors().add(this.actors.get(2).getId());
            this.films.add(film);
        }
        {
            Film film = new Film();
            film.setId(++indexFilms + "");
            film.setTitle("Le Seigneur des anneaux : Le Retour du roi");
            film.setReleaseYear(2003);
            film.setDirectorId(this.directors.get(0).getId());
            film.getActors().add(this.actors.get(0).getId());
            film.getActors().add(this.actors.get(1).getId());
            film.getActors().add(this.actors.get(2).getId());
            this.films.add(film);
        }
        {
            Film film = new Film();
            film.setId(++indexFilms + "");
            film.setTitle("Le Hobbit : Un voyage inattendu");
            film.setReleaseYear(2012);
            film.setDirectorId(this.directors.get(0).getId());
            film.getActors().add(this.actors.get(0).getId());
            film.getActors().add(this.actors.get(1).getId());
            this.films.add(film);
        }
        {
            Film film = new Film();
            film.setId(++indexFilms + "");
            film.setTitle("Le Hobbit : La Désolation de Smaug");
            film.setReleaseYear(2013);
            film.setDirectorId(this.directors.get(0).getId());
            film.getActors().add(this.actors.get(1).getId());
            this.films.add(film);
        }
        {
            Film film = new Film();
            film.setId(++indexFilms + "");
            film.setTitle("Le Hobbit : La Bataille des Cinq Armées");
            film.setReleaseYear(2014);
            film.setDirectorId(this.directors.get(0).getId());
            film.getActors().add(this.actors.get(1).getId());
            this.films.add(film);
        }
        {
            Film film = new Film();
            film.setId(++indexFilms + "");
            film.setTitle("La Grande Vadrouille");
            film.setReleaseYear(1966);
            film.setDirectorId(this.directors.get(1).getId());
            this.films.add(film);
        }

        {
            Film film = new Film();
            film.setId(++indexFilms + "");
            film.setTitle("Spirit, l'étalon des plaines");
            film.setReleaseYear(2002);
            film.setDirectorId(this.directors.get(2).getId());
            this.films.add(film);
        }

        {
            Film film = new Film();
            film.setId(++indexFilms + "");
            film.setTitle("Le Nombre 23");
            film.setReleaseYear(2007);
            film.setDirectorId(this.directors.get(3).getId());
            this.films.add(film);
        }

        {
            Film film = new Film();
            film.setId(++indexFilms + "");
            film.setTitle("Jeux d'enfants");
            film.setReleaseYear(2003);
            film.setDirectorId(this.directors.get(4).getId());
            this.films.add(film);
        }

    }


    // query
    
    @QueryMapping(name = "getFilms")
    public CompletableFuture<Iterable<Film>> getFilms(@Argument("search") String search) {
        return CompletableFuture.supplyAsync(() -> {
            System.out.println("start getFilms");
            // search filter
            if (search != null) {
                return this.films.stream().filter((film) -> film.getTitle().toLowerCase().contains(search.toLowerCase()))
                        .collect(Collectors.toList());
            }
            System.out.println("end getFilms");
            return this.films;
    }, this.executor);
    }

    //film resolver

    @SchemaMapping(typeName = "Film", field = "note")
    public CompletableFuture<Float> getNotationOfFilm(@Argument("reference") NoteReference note, Film film) {

        return CompletableFuture.supplyAsync(() -> {
            System.out.println("start getNotationOfFilm");
            var noteVeryGood = film.getTitle().length();
            if (note == NoteReference.SYLVAIN) {
                noteVeryGood = noteVeryGood / 2;
            }

            this.generateDelai();
            System.out.println("end getNotationOfFilm");
            return  Float.valueOf(noteVeryGood % 5);
        }, this.executor);
    }

    @SchemaMapping(typeName = "Film", field = "director")
    public CompletableFuture<Optional<Director>> getDirectorOfFilm(Film film) {
        return CompletableFuture.supplyAsync(() -> {
            System.out.println("start getDirectorOfFilm");
            this.generateDelai();
            System.out.println("end getDirectorOfFilm");
            return this.directors.stream().filter((director) -> director.getId() == film.getDirectorId()).findFirst();
        }, this.executor);
    }

    @SchemaMapping(typeName = "Film", field = "actors")
    public CompletableFuture<List<Actor>> getActorsrOfFilm(Film film) {
        return CompletableFuture.supplyAsync(() -> {
            System.out.println("start getActorsrOfFilm");
            this.generateDelai();
            
            System.out.println("end getActorsrOfFilm");
            return this.actors.stream().filter((actor) -> film.getActors().contains(actor.getId()))
                    .collect(Collectors.toList());
        }, this.executor);
    }


    //actor resolver
    @SchemaMapping(typeName = "Actor", field = "films")
    public CompletableFuture<List<Film>> getFilmsOfActor(@Argument("search") String search, Actor actor) {
        return CompletableFuture.supplyAsync(() -> {
            System.out.println("start getFilmsOfActor");
            var films = this.films.stream();
            // search filter
            if (search != null) {
                films = films.filter((film) -> film.getTitle().toLowerCase().contains(search.toLowerCase()));
            }

            System.out.println("end getFilmsOfActor");
            return films.filter((film) -> film.getActors().contains(actor.getId())).collect(Collectors.toList());
        }, this.executor);
    }


    //director resolver

    @SchemaMapping(typeName = "Director", field = "films")
    public CompletableFuture<List<Film>> getFilmsOfDirector(@Argument("search") String search, Director director) {
        return CompletableFuture.supplyAsync(() -> {
            System.out.println("start getFilmsOfDirector");
            var films = this.films.stream();
            // search filter
            if (search != null) {
                films = films.filter((film) -> film.getTitle().toLowerCase().contains(search.toLowerCase()));
            }

            System.out.println("end getFilmsOfDirector");
            return films.filter((film) -> film.getDirectorId() == director.getId()).collect(Collectors.toList());
        }, this.executor);
    }


    private void generateDelai(){
        var timeInMs = (int)(Math.random()*1000);
        try {
            Thread.sleep(timeInMs);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
