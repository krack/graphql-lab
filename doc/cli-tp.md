# GraphQL lab - client

## Utilisation de GraphiQL
Graphiql est un outil de création de requêtes avec la documentation associée au service graphql exposé.


### Path
Il est présent ici sur le path `/`, mais la valeur par défault est `/graphiql`

### Fonctionnalitées
- autocomplétion
- vérification syntaxique
- formatage
- documentation
- execution de requêtes

## Exercice 1 : Récupérer tous les films

<details>
  <summary>Récupérer <code>tous les films</code> avec leur <code>titre</code> et leur <code>année de parrution</code>.</summary>

```GraphQL
{
  getFilms{
    title
    releaseYear
  }
}
```

</details>

### Concept: 
- [Fields](https://graphql.org/learn/queries/#fields)

## Exercice 2 : Récupérer les films `Le Seigneur des anneaux`

<details>
  <summary>Récupérer seulement les <code>titres</code> des films <code>"Le Seigneur des anneaux"</code></summary>

```GraphQL
{
  getFilms(search: "Le Seigneur des anneaux"){
    title
  }
}
```

</details>

### Concept: 
- [Fields](https://graphql.org/learn/queries/#fields)
- [Arguments](https://graphql.org/learn/queries/#arguments)

## Exercice 3 : Récupérer les notations des films

<details>
  <summary>Récupérer le <code>titre</code> et la <code>note</code> des films</summary>

```GraphQL
{
  getFilms{
    title
    note
  }
}
```

</details>

<details>
  <summary>Récupérer le <code>titre</code> et la <code>note</code> des films selon la <code>reference</code> <code>ALLOCINE</code></summary>

```GraphQL
{
  getFilms{
    title
    note(reference: ALLOCINE)
  }
}
```

</details>

<details>
  <summary>Récupérer le <code>titre</code> et la <code>note</code> des films selon la <code>reference</code> <code>SYLVAIN</code></summary>

```GraphQL
{
  getFilms{
    title
    note(reference: SYLVAIN)
  }
}
```

</details>

### Concept: 
- [Arguments](https://graphql.org/learn/queries/#arguments)

## Exercice 4 : Récupérer les deux notations des films

<details>
  <summary>Récupérer le <code>titre</code> et la <code>note</code> des films selon les <code>reference</code> <code>ALLOCINE</code> et <code>SYLVAIN</code></summary>

```GraphQL
{
  getFilms{
    title
    noteSylvain: note(reference: SYLVAIN)
    noteAllocine: note(reference: ALLOCINE)
  }
}
```

</details>

### Concept: 
- [Arguments](https://graphql.org/learn/queries/#arguments)
- [Alias](https://graphql.org/learn/queries/#aliases)


## Exercice 5 : Récupérer les réalisateurs des films

<details>
  <summary>Récupérer le <code>titre</code> des films ainsi que le <code>nom</code> de leur <code>réalisateur</code></summary>

```GraphQL
{
  getFilms{
    title
    director{
      name
    }
  }
}
```

</details>


### Concept: 
- [Fields](https://graphql.org/learn/queries/#fields)


## Exercice 6 : Récupérer les films du réalisateur d'un film

<details>
  <summary>Récupérer les <code>titres</code> et <code>notes</code> de <code>Sylvain</code> des films du <code>réalisateur</code> du film <code>Le Seigneur des anneaux : Les Deux Tours</code> </summary>

```GraphQL
{
  getFilms(search: "Le Seigneur des anneaux : Les Deux Tours"){
    director{
      films{
        title
        note(reference: SYLVAIN)
      }
    }
  }
}
```

</details>

### Concept: 
- [Fields](https://graphql.org/learn/queries/#fields)
- [Arguments](https://graphql.org/learn/queries/#arguments)

## Exercice final

<details>
  <summary>Récupérer les titres des films de la Saga <code>Le Seigneur des anneaux</code>, ayant pour réalisateur le réalisateur du <code>Le Seigneur des anneaux : Les Deux Tours</code></summary>

```GraphQL
{
  getFilms(search: "Le Seigneur des anneaux : Les Deux Tours"){
    director{
      films(search: "Le Seigneur des anneaux"){
        title
      }
    }
  }
}
```

</details>
